﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraTab;


namespace UserControlLibrary
{
    public partial class UserWizardControl : UserControl
    {
        public UserWizardControl()
        {
            InitializeComponent();

        }

        private void xtraTabPage1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void WizardTabsControl_Click(object sender, EventArgs e)
        {

        }


        public void SetCurrentPage(XtraTabPage TabPage)
        {
            WizardTabsControl.SelectedTabPage = TabPage;
        }

        public XtraTabPage GetCurrentPage()
        {
            return this.WizardTabsControl.SelectedTabPage;
        }

        public XtraTabPage GetPage1()
        {
            int test = WizardTabsControl.TabPages.Count;
            return this.WizardTabsControl.TabPages[0];
        }

        public XtraTabPage GetPage2()
        {

            return this.WizardTabsControl.TabPages[1];
        }

        public XtraTabPage GetPage3()
        {
            return this.WizardTabsControl.TabPages[2];
        }

        public XtraTabControl GetTabControl()
        {
            return this.WizardTabsControl;
        }






    }

}
