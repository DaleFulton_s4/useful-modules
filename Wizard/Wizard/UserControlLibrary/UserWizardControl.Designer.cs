﻿namespace UserControlLibrary
{
    partial class UserWizardControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WizardTabsControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.lblHeading = new System.Windows.Forms.Label();
            this.bgInstructions = new System.Windows.Forms.GroupBox();
            this.lblInstructions = new System.Windows.Forms.Label();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.wizardPageControl1 = new Wizard.WizardPageControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.wizardPageControl2 = new Wizard.WizardPageControl();
            ((System.ComponentModel.ISupportInitialize)(this.WizardTabsControl)).BeginInit();
            this.WizardTabsControl.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.bgInstructions.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // WizardTabsControl
            // 
            this.WizardTabsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WizardTabsControl.Location = new System.Drawing.Point(0, 0);
            this.WizardTabsControl.Name = "WizardTabsControl";
            this.WizardTabsControl.SelectedTabPage = this.xtraTabPage1;
            this.WizardTabsControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.WizardTabsControl.Size = new System.Drawing.Size(1262, 752);
            this.WizardTabsControl.TabIndex = 5;
            this.WizardTabsControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.lblHeading);
            this.xtraTabPage1.Controls.Add(this.bgInstructions);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1255, 745);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // lblHeading
            // 
            this.lblHeading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHeading.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeading.Font = new System.Drawing.Font("Constantia", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(0, 0);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(1255, 47);
            this.lblHeading.TabIndex = 9;
            this.lblHeading.Text = "Heading";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bgInstructions
            // 
            this.bgInstructions.Controls.Add(this.lblInstructions);
            this.bgInstructions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bgInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bgInstructions.Location = new System.Drawing.Point(0, 683);
            this.bgInstructions.Name = "bgInstructions";
            this.bgInstructions.Size = new System.Drawing.Size(1255, 62);
            this.bgInstructions.TabIndex = 8;
            this.bgInstructions.TabStop = false;
            this.bgInstructions.Text = "Instructions";
            // 
            // lblInstructions
            // 
            this.lblInstructions.AllowDrop = true;
            this.lblInstructions.AutoSize = true;
            this.lblInstructions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInstructions.Font = new System.Drawing.Font("Microsoft YaHei Light", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstructions.Location = new System.Drawing.Point(3, 23);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(231, 20);
            this.lblInstructions.TabIndex = 1;
            this.lblInstructions.Text = "Type Page instructions here please ";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.wizardPageControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1255, 718);
            this.xtraTabPage2.Text = "xtraTabPage2";
            // 
            // wizardPageControl1
            // 
            this.wizardPageControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizardPageControl1.Location = new System.Drawing.Point(0, 0);
            this.wizardPageControl1.Name = "wizardPageControl1";
            this.wizardPageControl1.Size = new System.Drawing.Size(1255, 718);
            this.wizardPageControl1.TabIndex = 0;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.wizardPageControl2);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1255, 718);
            this.xtraTabPage3.Text = "xtraTabPage3";
            // 
            // wizardPageControl2
            // 
            this.wizardPageControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizardPageControl2.Location = new System.Drawing.Point(0, 0);
            this.wizardPageControl2.Name = "wizardPageControl2";
            this.wizardPageControl2.Size = new System.Drawing.Size(1255, 718);
            this.wizardPageControl2.TabIndex = 0;
            // 
            // UserWizardControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.WizardTabsControl);
            this.Name = "UserWizardControl";
            this.Size = new System.Drawing.Size(1262, 752);
            ((System.ComponentModel.ISupportInitialize)(this.WizardTabsControl)).EndInit();
            this.WizardTabsControl.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.bgInstructions.ResumeLayout(false);
            this.bgInstructions.PerformLayout();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl WizardTabsControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.GroupBox bgInstructions;
        private System.Windows.Forms.Label lblInstructions;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private Wizard.WizardPageControl wizardPageControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private Wizard.WizardPageControl wizardPageControl2;
    }
}
