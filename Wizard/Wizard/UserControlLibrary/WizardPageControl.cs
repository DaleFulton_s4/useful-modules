﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wizard
{
    public partial class WizardPageControl : UserControl
    {
        public WizardPageControl()
        {
            InitializeComponent();
            
        }

        private void bgInstructions_Enter(object sender, EventArgs e)
        {

        }

        private void lblHeading_Click(object sender, EventArgs e)
        {

        }

        private void lblInstructions_Click(object sender, EventArgs e)
        {

        }

        private void WizardPageControl_Load(object sender, EventArgs e)
        {

        }
        public void Instructions(string message)
        {
            lblInstructions.Text = message;
        }

        public void Heading(string message)
        {
            lblHeading.Text = message;
        }
    }
}
