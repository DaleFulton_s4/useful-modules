﻿
namespace Wizard
{
    partial class WizardPageControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblHeading = new System.Windows.Forms.Label();
            this.bgInstructions = new System.Windows.Forms.GroupBox();
            this.lblInstructions = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.bgInstructions.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblHeading);
            this.panel1.Controls.Add(this.bgInstructions);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(724, 101);
            this.panel1.TabIndex = 5;
            // 
            // lblHeading
            // 
            this.lblHeading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHeading.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeading.Font = new System.Drawing.Font("Constantia", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(0, 0);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(724, 47);
            this.lblHeading.TabIndex = 7;
            this.lblHeading.Text = "Heading";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bgInstructions
            // 
            this.bgInstructions.Controls.Add(this.lblInstructions);
            this.bgInstructions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bgInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bgInstructions.Location = new System.Drawing.Point(0, 50);
            this.bgInstructions.Name = "bgInstructions";
            this.bgInstructions.Size = new System.Drawing.Size(724, 51);
            this.bgInstructions.TabIndex = 6;
            this.bgInstructions.TabStop = false;
            this.bgInstructions.Text = "Instructions";
            // 
            // lblInstructions
            // 
            this.lblInstructions.AllowDrop = true;
            this.lblInstructions.AutoSize = true;
            this.lblInstructions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInstructions.Font = new System.Drawing.Font("Microsoft YaHei Light", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstructions.Location = new System.Drawing.Point(3, 23);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(231, 20);
            this.lblInstructions.TabIndex = 1;
            this.lblInstructions.Text = "Type Page instructions here please ";
            this.lblInstructions.Click += new System.EventHandler(this.lblInstructions_Click);
            // 
            // WizardPageControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "WizardPageControl";
            this.Size = new System.Drawing.Size(724, 450);
            this.Load += new System.EventHandler(this.WizardPageControl_Load);
            this.panel1.ResumeLayout(false);
            this.bgInstructions.ResumeLayout(false);
            this.bgInstructions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.GroupBox bgInstructions;
        private System.Windows.Forms.Label lblInstructions;
    }
}
