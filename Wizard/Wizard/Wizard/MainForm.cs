﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using nsEventDistributer;
using nsEventTypes;

namespace Wizard
{

    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EventDistributer.SubscribeForEvent(EventType.coStartWizard, HandledEvent_GenerateWizard);
        }

     
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            EventDistributer.Publish(new StartWizardEvent());
        }

        public void HandledEvent_GenerateWizard(BaseEvent Event)
        {
            if (Event is StartWizardEvent)
            {
          
                // call form with user control on it.
                WizardForm F1 = new WizardForm();
                F1.ShowDialog();

            }
        }
    }
}
