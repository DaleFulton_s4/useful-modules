﻿using System;
using StateDesignPattern;
using nsEventDistributer;
using nsEventTypes;

namespace ExampleStates
{
    /// <summary>
    /// A 'ConcreteState' class
    /// </summary>
    class Page1 : State
    {
        public Page1(State paState) : base(paState) { }
        public Page1(StateMachine paStateMachine) : base(paStateMachine) { }

        // Handle event is subscriber methods "event handler method"
        public override void HandleEvent(BaseEvent evt)
        {
            switch (evt.Type)
            {
                case EventType.coENTRY:
                    {
                        Console.WriteLine("ENTRY in : " + this.ToString());
                        EventDistributer.Publish(new Page1Event());
                        return;
                    }
                case EventType.coEXIT:
                    {
                        Console.WriteLine("EXIT from: " + this.ToString());
                        return;
                    }
                case EventType.coNextClicked:
                    {
                        Transition(new Page2(this));
                        return;
                    }
                case EventType.coPreviousClicked:
                    {           
                        return;
                    }
                case EventType.coRetry:
                    {
                        Transition(new Page1(this));
                        return;
                    }
                default:
                    return;
            }
        }
    }

    /// <summary>
    /// A 'ConcreteState' class
    /// </summary>
    class Page2 : State
    {
        // Overloaded Constructors
        public Page2(State paState) : base(paState) { }
        public Page2(StateMachine paStateMachine) : base(paStateMachine) { }

        public override void HandleEvent(BaseEvent evt)
        {
            switch (evt.Type)
            {
                case EventType.coENTRY:
                    {
                        Console.WriteLine("ENTRY in : " + this.ToString());
                        EventDistributer.Publish(new Page2Event());
                        return;
                    }
                case EventType.coEXIT:
                    {
                        Console.WriteLine("EXIT from: " + this.ToString());
                        ExitEvent e = (ExitEvent)evt;

                        return;
                    }
                case EventType.coNextClicked:
                    {
                        Transition(new Page3(this));
                        return;
                    }
                case EventType.coPreviousClicked:
                    {
                        Transition(new Page1(this));
                        return;
                    }
                case EventType.coRetry:
                    {
                        Transition(new Page2(this));
                        return;
                    }
                default:
                    return;
            }
        }
    }

    /// <summary>
    /// A 'ConcreteState' class
    /// </summary>
    class Page3 : State
    {
        // Overloaded Constructors
        public Page3(State paState) : base(paState) { }
        public Page3(StateMachine paStateMachine) : base(paStateMachine) { }

        public override void HandleEvent(BaseEvent evt)
        {
            switch (evt.Type)
            {
                case EventType.coENTRY:
                    {
                        Console.WriteLine("ENTRY in : " + this.ToString());
                        EventDistributer.Publish(new Page3Event());
                        return;
                    }
                case EventType.coEXIT:
                    {
                        Console.WriteLine("EXIT from: " + this.ToString());
                        Console.WriteLine("Export Data to be saved " + this.ToString());
                        return;
                    }
                case EventType.coNextClicked:
                    {
                        EventDistributer.Publish(new CloseWizardEvent());
                        
                        return;
                    }
                case EventType.coPreviousClicked:
                    {
                        Transition(new Page2(this));
                        return;
                    }
                case EventType.coRetry:
                    {
                        Transition(new Page3(this));
                        return;
                    }
                default:
                    return;
            }
        }
    }
}