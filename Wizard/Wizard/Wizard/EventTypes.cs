﻿using nsEventDistributer;

namespace nsEventTypes
{
    public enum EventType
    {
        coENTRY,
        coEXIT,
        coNextClicked,
        coPreviousClicked,
        coRetry,
        coPage1,
        coPage2,
        coPage3,
        coStartWizard,
        coCloseWizard
    }

    // Custom events
    // Events as classes as properties can send data 

    //Event: Initialise and load Wizard Form
     public class StartWizardEvent : BaseEvent
    {
        public StartWizardEvent() { Type = EventType.coStartWizard; }
    }
    public class CloseWizardEvent : BaseEvent
    {
        public CloseWizardEvent() { Type = EventType.coCloseWizard; }
    }

    //State Machine Events
    public class EntryEvent : BaseEvent
    {
        public EntryEvent() { Type = EventType.coENTRY; }
    }

    public class ExitEvent : BaseEvent
    {
        public ExitEvent() { Type = EventType.coEXIT; }
    }

    public class RetryEvent : BaseEvent
    {
        public RetryEvent() { Type = EventType.coRetry; }
    }
    public class NextClickedEvent : BaseEvent
    {
        public NextClickedEvent() { Type = EventType.coNextClicked; }
    }

    public class PreviousClickedEvent : BaseEvent
    {
        public PreviousClickedEvent() { Type = EventType.coPreviousClicked; }
    }

    public class Page1Event : BaseEvent
    {
        public Page1Event() { Type = EventType.coPage1; }
    }

    public class Page2Event: BaseEvent
    {
        public Page2Event() { Type = EventType.coPage2; }
    }

    public class Page3Event : BaseEvent
    {
        public Page3Event() { Type = EventType.coPage3; }
    }



}
