﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StateDesignPattern;
using ExampleStates;
using nsEventDistributer;
using nsEventTypes;
using System.Threading;


namespace Wizard
{
    public partial class WizardForm : Form
    {
        
        StateMachine loStateMachine;
        public WizardForm()
        {
            InitializeComponent();
        }

        private void TestWizardForm_Load(object sender, EventArgs e)
        {

            loStateMachine = new StateMachine();

            EventDistributer.SubscribeForEvent(EventType.coPage1, HandleEvent_Page1);
            EventDistributer.SubscribeForEvent(EventType.coPage2, HandleEvent_Page2);
            EventDistributer.SubscribeForEvent(EventType.coPage3, HandleEvent_Page3);

            EventDistributer.SubscribeForEvent(EventType.coNextClicked, loStateMachine.HandleEvent);
            EventDistributer.SubscribeForEvent(EventType.coPreviousClicked, loStateMachine.HandleEvent);
            EventDistributer.SubscribeForEvent(EventType.coRetry, loStateMachine.HandleEvent);
            EventDistributer.SubscribeForEvent(EventType.coCloseWizard, HandleEvent_CloseWizard);
            // set initial state of state machine
            loStateMachine.SetInitialState(new Page1(loStateMachine));
        }

        public void HandleEvent_CloseWizard(BaseEvent evt)
        {
            if(evt is CloseWizardEvent)
            {
                EventDistributer.UnsubscribeAllEvents( HandleEvent_Page1);
                EventDistributer.UnsubscribeAllEvents( HandleEvent_Page2);
                EventDistributer.UnsubscribeAllEvents(HandleEvent_Page3);

                EventDistributer.UnsubscribeAllEvents(loStateMachine.HandleEvent);
              

                EventDistributer.UnsubscribeAllEvents(HandleEvent_CloseWizard);
               
                UWC.Dispose();
                this.Dispose();
                this.Close();                
            }
        }

        public void HandleEvent_Page1(BaseEvent evt)
        {
            // set tab page 1
            if (evt is Page1Event)
            {
                this.Refresh();
                UWC.SetCurrentPage(UWC.GetPage1());
                UWC.GetCurrentPage().ForeColor = Color.Green;
                this.Refresh();
                Thread.Sleep(1000);
                UWC.GetCurrentPage().ForeColor = Color.Black;
            }
        }

        public void HandleEvent_Page2(BaseEvent evt)
        {
            // set tab page 2
            if (evt is Page2Event)
            {
                UWC.SetCurrentPage(UWC.GetPage2());
                UWC.GetCurrentPage().ForeColor = Color.Yellow;
                this.Refresh();
                Thread.Sleep(1000);
                UWC.GetCurrentPage().ForeColor = Color.Black;
            }
        }

        public void HandleEvent_Page3(BaseEvent evt)
        {
            // set tab page 3
            if (evt is Page3Event)
            {
                UWC.SetCurrentPage(UWC.GetPage3());
                UWC.GetCurrentPage().ForeColor = Color.Red;
                this.Refresh();
                Thread.Sleep(1000);
                UWC.GetCurrentPage().ForeColor = Color.Black;
            }
        }

        private void UWC_Load(object sender, EventArgs e)
        {

        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            EventDistributer.Publish(new NextClickedEvent());
        }

        private void btnRetry_Click(object sender, EventArgs e)
        {
            EventDistributer.Publish(new RetryEvent());
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            EventDistributer.Publish(new PreviousClickedEvent());
        }

        private void userWizardControl1_Load(object sender, EventArgs e)
        {

        }
    }
}
