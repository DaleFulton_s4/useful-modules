﻿using System;
using System.Collections.Generic;
using nsEventTypes;

namespace nsEventDistributer
{



    public abstract class BaseEvent
    {
        private EventType meType;

        public EventType Type
        {
            get { return meType; }
            set { meType = value; }
        }
    }


    // Handles differently to c# events and delegates. Uses a dictionary to save events and corresponding callback functions. When even it published it looks for functions in dictionary and calls them.
    public static class EventDistributer
    {
        // create delegate with same signature as subcriber methods.
        public delegate void EventDelegate(BaseEvent paEvent);
        // create dictionary with events as keys and a list of subscriber methods subscribed as elements. Acts as a list to keep track of subscriptions
        static SortedDictionary<EventType, List<EventDelegate>> meSubscribersDict = new SortedDictionary<EventType, List<EventDelegate>>();
        // create lock object 
        static readonly Object meLocker = new Object();

        // subscribe a function to an event
        public static void SubscribeForEvent(EventType paType, EventDelegate paEventDelegate)
        {
            // lock subscription for multi threaded applications
            lock (meLocker)
            {
                // if event is in dictonary (do we have key in dictionary) i.e. we have subscribed to event before.
                if (meSubscribersDict.ContainsKey(paType))
                {
                    // if nothing subscribed i.e. if subscriber function is not in list then add it.
                    if (!meSubscribersDict[paType].Contains(paEventDelegate))
                    {
                        //we add to list.
                        meSubscribersDict[paType].Add(paEventDelegate);
                    }
                }
                else
                {
                    //make new list that contains new delegate
                    List<EventDelegate> loList = new List<EventDelegate>
                    {
                        paEventDelegate
                    };
                    // add new list and key to dictionary
                    meSubscribersDict.Add(paType, loList);
                }
            }
        }

        public static void UnsubscribeFromEvent(EventType paType, EventDelegate paEventDelegate)
        {
            lock (meLocker)
            {
                if (meSubscribersDict.ContainsKey(paType))
                {
                    List<EventDelegate> loSubscribers = meSubscribersDict[paType];
                    loSubscribers.RemoveAll(item => item == paEventDelegate);
                }

                //else
                //{
                //    // nothing subcribed to paEvent. Nothing to unsubscribe.
                //}
            }
        }

        public static void UnsubscribeAllEvents(EventDelegate paEventDelegate)
        {
            lock (meLocker)
            {
                foreach (KeyValuePair<EventType, List<EventDelegate>> loPair in meSubscribersDict)
                {
                    loPair.Value.RemoveAll(item => item == paEventDelegate);
                }
            }
        }

        // fires an event. looks in dictionary, until it finds key, it then retreive list and calls functions
        public static void Publish(BaseEvent paEvent)
        {
            lock (meLocker)
            {

                EventType loType = paEvent.Type;
                if (meSubscribersDict.ContainsKey(loType))
                {
                    List<EventDelegate> loSubscribers = meSubscribersDict[loType];

                    //this does not work as unsibscribing the current published event causes issues, use generic for loop below.
                    //foreach (EventDelegate loSubscriber in loSubscribers)
                    //{
                    //    loSubscriber(paEvent);

                    //}

                    for(int i = 0; i < loSubscribers.Count;i++)
                    {
                        loSubscribers[i](paEvent);
                    }
                }

                //else
                //{
                //    // nothing subcribed to paEvent. Nothing to publish.
                //}
            }
        }
    }
}
