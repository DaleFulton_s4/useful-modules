﻿namespace StatePatternExampleGui
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelRed = new System.Windows.Forms.Label();
            this.labelAmber = new System.Windows.Forms.Label();
            this.labelGreen = new System.Windows.Forms.Label();
            this.ButtonNext = new System.Windows.Forms.Button();
            this.ButtonPrevious = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelRed
            // 
            this.labelRed.Location = new System.Drawing.Point(16, 15);
            this.labelRed.Margin = new System.Windows.Forms.Padding(4);
            this.labelRed.Name = "labelRed";
            this.labelRed.Size = new System.Drawing.Size(267, 28);
            this.labelRed.TabIndex = 0;
            // 
            // labelAmber
            // 
            this.labelAmber.Location = new System.Drawing.Point(16, 50);
            this.labelAmber.Margin = new System.Windows.Forms.Padding(4);
            this.labelAmber.Name = "labelAmber";
            this.labelAmber.Size = new System.Drawing.Size(267, 28);
            this.labelAmber.TabIndex = 1;
            // 
            // labelGreen
            // 
            this.labelGreen.Location = new System.Drawing.Point(16, 86);
            this.labelGreen.Margin = new System.Windows.Forms.Padding(4);
            this.labelGreen.Name = "labelGreen";
            this.labelGreen.Size = new System.Drawing.Size(267, 28);
            this.labelGreen.TabIndex = 2;
            this.labelGreen.Click += new System.EventHandler(this.labelGreen_Click);
            // 
            // ButtonNext
            // 
            this.ButtonNext.Location = new System.Drawing.Point(20, 122);
            this.ButtonNext.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonNext.Name = "ButtonNext";
            this.ButtonNext.Size = new System.Drawing.Size(132, 28);
            this.ButtonNext.TabIndex = 3;
            this.ButtonNext.Text = "Next";
            this.ButtonNext.UseVisualStyleBackColor = true;
            this.ButtonNext.Click += new System.EventHandler(this.ButtonNext_Click);
            // 
            // ButtonPrevious
            // 
            this.ButtonPrevious.Location = new System.Drawing.Point(160, 122);
            this.ButtonPrevious.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonPrevious.Name = "ButtonPrevious";
            this.ButtonPrevious.Size = new System.Drawing.Size(132, 28);
            this.ButtonPrevious.TabIndex = 4;
            this.ButtonPrevious.Text = "Previous";
            this.ButtonPrevious.UseVisualStyleBackColor = true;
            this.ButtonPrevious.Click += new System.EventHandler(this.ButtonPrevious_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 161);
            this.Controls.Add(this.ButtonPrevious);
            this.Controls.Add(this.ButtonNext);
            this.Controls.Add(this.labelGreen);
            this.Controls.Add(this.labelAmber);
            this.Controls.Add(this.labelRed);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelRed;
        private System.Windows.Forms.Label labelAmber;
        private System.Windows.Forms.Label labelGreen;
        private System.Windows.Forms.Button ButtonNext;
        private System.Windows.Forms.Button ButtonPrevious;
    }
}

