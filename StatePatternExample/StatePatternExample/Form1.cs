﻿using System;
using System.Drawing;
using System.Windows.Forms;
using nsEventDistributer;
using nsEventTypes;

namespace StatePatternExampleGui
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonNext_Click(object sender, EventArgs e)
        {
            EventDistributer.Publish(new NextClickedEvent());
        }

        private void ButtonPrevious_Click(object sender, EventArgs e)
        {
            EventDistributer.Publish(new PreviousClickedEvent());
        }

        public void ActivateRed()
        {
            labelRed.BackColor   = Color.Red;
            labelAmber.BackColor = Color.Black;
            labelGreen.BackColor = Color.Black;
        }

        public void ActivateAmber()
        {
            labelRed.BackColor   = Color.Black;
            labelAmber.BackColor = Color.Orange;
            labelGreen.BackColor = Color.Black;
        }

        public void ActivateGreen()
        {
            labelRed.BackColor   = Color.Black;
            labelAmber.BackColor = Color.Black;
            labelGreen.BackColor = Color.Green;
        }

        public void HandleEvent(BaseEvent evt)
        {
            switch (evt.Type)
            {
                case EventType.coActivateRed:
                    ActivateRed();
                    return;
                case EventType.coActivateAmber:
                    ActivateAmber();
                    return;
                case EventType.coActivateGreen:
                    ActivateGreen();
                    return;
            }
        }

        private void labelGreen_Click(object sender, EventArgs e)
        {

        }
    }
}
