﻿/* 
 * This is a C# implementation of the State Design Pattern.
 * It is based on the example found at http://www.dofactory.com/net/state-design-pattern
 */

using nsEventDistributer;
using nsEventTypes;

namespace StateDesignPattern
{
    /// <summary>
    /// The 'State' abstract class
    /// </summary>
    abstract class State
    {
        protected StateMachine meStateMachine;

        // Overloaded Constructors
        public State(State paState)
        {
            meStateMachine = paState.StateMachine;
        }

        public State(StateMachine paStateMachine)
        {
            meStateMachine = paStateMachine;
        }

        // Properties
        private StateMachine StateMachine
        {
            get { return meStateMachine; }
            //set { meStateMachine = value; } // not needed
        }

        protected void Transition(State paNextState)
        {
            // Handle EXIT of current state
            EventDistributer.SubscribeForEvent(EventType.coEXIT, meStateMachine.CurrentState.HandleEvent);
            EventDistributer.Publish(new ExitEvent());
            EventDistributer.UnsubscribeFromEvent(EventType.coEXIT, meStateMachine.CurrentState.HandleEvent);

            meStateMachine.CurrentState = paNextState;
            // Handle ENTRY event of new state
            EventDistributer.SubscribeForEvent(EventType.coENTRY, meStateMachine.CurrentState.HandleEvent);
            EventDistributer.Publish(new EntryEvent());
            EventDistributer.UnsubscribeFromEvent(EventType.coENTRY, meStateMachine.CurrentState.HandleEvent);           
        }

        public abstract void HandleEvent(BaseEvent evt);
    }

    

    /// <summary>
    /// The 'Context' class
    /// </summary>
    class StateMachine
    {
        private State meCurrentState;
        
        // Constructor
        public StateMachine() { }

        // Properties
        public State CurrentState
        {
            get { return meCurrentState; }
            set { meCurrentState = value; }
        }

        public void SetInitialState(State paState)
        {
            meCurrentState = paState;

            // Handle ENTRY event
            EventDistributer.SubscribeForEvent(EventType.coENTRY, meCurrentState.HandleEvent);
            EventDistributer.Publish(new EntryEvent());
            EventDistributer.UnsubscribeFromEvent(EventType.coENTRY, meCurrentState.HandleEvent);
        }

        public void HandleEvent(BaseEvent evt)
        {
            meCurrentState.HandleEvent(evt);
        }
    }
}
