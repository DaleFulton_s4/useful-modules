﻿namespace nsEventTypes
{
    public enum EventType
    {
        coENTRY,
        coEXIT,
        coNextClicked,
        coPreviousClicked,
        coActivateRed,
        coActivateAmber,
        coActivateGreen
    }

    public abstract class BaseEvent
    {
        private EventType meType;

        public EventType Type
        {
            get { return meType;  }
            set { meType = value; }
        }
    }

    // Custom events
    // Events as classes as properties can send data 
    class EntryEvent : BaseEvent
    {
        public EntryEvent() { Type = EventType.coENTRY; }
    }

    class ExitEvent : BaseEvent
    {
        public ExitEvent() { Type = EventType.coEXIT; }
    }

    class NextClickedEvent : BaseEvent
    {
        public NextClickedEvent() { Type = EventType.coNextClicked; }
    }

    class PreviousClickedEvent : BaseEvent
    {
        public PreviousClickedEvent() { Type = EventType.coPreviousClicked; }
    }

    class ActivateRedEvent : BaseEvent
    {
        public ActivateRedEvent() { Type = EventType.coActivateRed; }
    }

    class ActivateAmberEvent : BaseEvent
    {
        public ActivateAmberEvent() { Type = EventType.coActivateAmber; }
    }

    class ActivateGreenEvent : BaseEvent
    {
        public ActivateGreenEvent() { Type = EventType.coActivateGreen; }
    }
}
