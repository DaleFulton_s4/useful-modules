﻿using System;
using nsEventDistributer;
using nsEventTypes;
using StateDesignPattern;

namespace ExampleStates
{
    /// <summary>
    /// A 'ConcreteState' class
    /// </summary>
    class RedState : State
    {
        public RedState(State paState) : base(paState) { }
        public RedState(StateMachine paStateMachine) : base(paStateMachine) { }

        // Handle event is subscriber methods "event handler method"
        public override void HandleEvent(BaseEvent evt)
        {
            switch (evt.Type)
            {
                case EventType.coENTRY:
                    {
                        Console.WriteLine("ENTRY in : " + this.ToString());
                        EventDistributer.Publish(new ActivateRedEvent());
                        return;
                    }
                case EventType.coEXIT:
                    {
                        Console.WriteLine("EXIT from: " + this.ToString());
                        return;
                    }
                case EventType.coNextClicked:
                    {
                        Transition(new GreenState(this));
                        return;
                    }
                case EventType.coPreviousClicked:
                    {
                        Transition(new AmberState(this));
                        return;
                    }
                default:
                    return;
            }
        }
    }

    /// <summary>
    /// A 'ConcreteState' class
    /// </summary>
    class GreenState : State
    {
        // Overloaded Constructors
        public GreenState(State paState) : base(paState) { }
        public GreenState(StateMachine paStateMachine) : base(paStateMachine) { }

        public override void HandleEvent(BaseEvent evt)
        {
            switch (evt.Type)
            {
                case EventType.coENTRY:
                    {
                        Console.WriteLine("ENTRY in : " + this.ToString());
                        EventDistributer.Publish(new ActivateGreenEvent());
                        return;
                    }
                case EventType.coEXIT:
                    {
                        Console.WriteLine("EXIT from: " + this.ToString());
                        ExitEvent e = (ExitEvent)evt;

                        return;
                    }
                case EventType.coNextClicked:
                    {
                        Transition(new AmberState(this));
                        return;
                    }
                case EventType.coPreviousClicked:
                    {
                        Transition(new RedState(this));
                        return;
                    }
                default:
                    return;
            }
        }
    }

    /// <summary>
    /// A 'ConcreteState' class
    /// </summary>
    class AmberState : State
    {
        // Overloaded Constructors
        public AmberState(State paState) : base(paState) { }
        public AmberState(StateMachine paStateMachine) : base(paStateMachine) { }

        public override void HandleEvent(BaseEvent evt)
        {
            switch (evt.Type)
            {
                case EventType.coENTRY:
                    {
                        Console.WriteLine("ENTRY in : " + this.ToString());
                        EventDistributer.Publish(new ActivateAmberEvent());
                        return;
                    }
                case EventType.coEXIT:
                    {
                        Console.WriteLine("EXIT from: " + this.ToString());
                        return;
                    }
                case EventType.coNextClicked:
                    {
                        Transition(new RedState(this));
                        return;
                    }
                case EventType.coPreviousClicked:
                    {
                        Transition(new GreenState(this));
                        return;
                    }
                default:
                    return;
            }
        }
    }
}