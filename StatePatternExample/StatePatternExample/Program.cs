﻿using System;
using System.Windows.Forms;
using StatePatternExampleGui;
using StateDesignPattern;
using ExampleStates;
using nsEventDistributer;
using nsEventTypes;

namespace StatePatternExample
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Form1 loForm = new Form1();
            
            //subscribe events for changing form colours
            EventDistributer.SubscribeForEvent(EventType.coActivateRed,   loForm.HandleEvent);
            EventDistributer.SubscribeForEvent(EventType.coActivateAmber, loForm.HandleEvent);
            EventDistributer.SubscribeForEvent(EventType.coActivateGreen, loForm.HandleEvent);
   
            StateMachine loStateMachine = new StateMachine();

            EventDistributer.SubscribeForEvent(EventType.coNextClicked, loStateMachine.HandleEvent);
            EventDistributer.SubscribeForEvent(EventType.coPreviousClicked, loStateMachine.HandleEvent);

            // set initial state of state machine
            loStateMachine.SetInitialState(new GreenState(loStateMachine));

            Application.Run(loForm);
        }
    }
}
