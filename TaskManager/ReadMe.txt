The task manager manages tasks to be executed. All tasks are managed by a queue system (FIFO). Tasks can be placed on the queue, or removed from the queue and executed.
Tasks are generated through inheritance, therefore they can be overriden to any specific task then executed.
