﻿using NLog;
using System;
using System.Collections.Concurrent;

namespace Lib.TaskManager
{
    public sealed class TaskManager
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly Lazy<TaskManager> taskManager = new Lazy<TaskManager>(() => new TaskManager());
        
        private static ConcurrentQueue<BaseTask> tasks = new ConcurrentQueue<BaseTask>();
        private bool isBusy = false;


        private TaskManager()
        {
        }

        public static TaskManager Instance
        {
            get { return taskManager.Value; }
        }

        public void Clear()
        {
            tasks = new ConcurrentQueue<BaseTask>();
        }

        public void RunConcurrently(BaseTask task)
        {
            throw new NotImplementedException();
        }

        public void RunSequentially(BaseTask task)
        {
            if ((tasks.Count < 1) && !isBusy)
            {
                Run(task);
            }
            else
            {
                logger.Trace("Enqueue: Task" + task.ID);
                tasks.Enqueue(task);
            }
        }

        private void Run(BaseTask task)
        {
            isBusy = true;
            task.TaskCompleted += Task_TaskCompleted;
            task.Run();
        }

        // callback function of task complete in tasks.cs
        private void Task_TaskCompleted(object sender, EventArgs e)
        {
            if ((sender is BaseTask task))
            {
                isBusy = false;
                task.TaskCompleted -= Task_TaskCompleted;

                if (tasks.Count > 0)
                {
                    while (tasks.TryDequeue(out BaseTask nextTask))
                    {
                        logger.Trace("Dequeue and run: Task" + nextTask.ID);
                        Run(nextTask);
                        break;
                    }
                }
                else
                {
                    logger.Trace("No pending tasks");
                }
            }
        }
    }
}