﻿using NLog;
using System;
using System.Threading;

namespace Lib.TaskManager
{
    public abstract class BaseTask
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private static long counter = 0;

        public BaseTask()
        {
            counter++;
            ID = counter;
        }

        public event EventHandler TaskCompleted;

        public long ID { get; }
        public bool Completed { get; private set; } = false;

        internal void Run()
        {
            Thread thread = new Thread(() =>
            {
                //try
                //{
                //    logger.Trace("Task " + ID.ToString() + ": STARTED");
                Completed = ExecuteImpl();
                //    Completed = true;
                //}
                //catch (Exception ex)
                //{
                //    logger.Error("Task " + ID.ToString() + ": " + ex);
                //    Completed = false;
                //}
                OnTaskCompleted();
            })
            {
                IsBackground = true
            };
            thread.Start();
        }

        protected abstract bool ExecuteImpl();

        private void OnTaskCompleted()
        {
            string log = "Task " + ID.ToString() + ": COMPLETED";
            if (Completed)
            {
                log += " successfully";
            }
            else
            {
                log += " with error(s)";
            }
            logger.Trace(log);
            // fire event TaskCompleted
            TaskCompleted?.Invoke(this, new EventArgs());
        }
    }
}